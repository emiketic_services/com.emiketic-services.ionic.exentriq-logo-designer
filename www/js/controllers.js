angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
  // Form data for the login modal
  $scope.startData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/start.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeStart = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.start = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doStart = function() {
    console.log('Saving title...', $scope.startData.name);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeStart();
    }, 500);
  };

  $scope.shapes = [
    { image: 'img/checkbox1.png', id: 1},
    { image: 'img/checkbox2.png', id: 2},
    { image: 'img/circle.png', id: 3},
    { image: 'img/polygonal.png', id: 4},
    { image: 'img/rectangle1.png', id: 5}
  ];

    // Perform the login action when the user submits the login form
  $scope.pick = function(id) {
    $scope.shapeData = $scope.shapes[id - 1];
  };

})

.controller('DesignerCtrl', function($scope) {
  
})

.controller('DesignerCtrl', function($scope, $stateParams) {
});
